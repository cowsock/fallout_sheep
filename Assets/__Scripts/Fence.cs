﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Fence : NetworkBehaviour {

    [SyncVar (hook = "OnPickedUp")]
    public bool isPickedUp;

    Collider2D coll;

    void Awake(){
        coll = GetComponent<Collider2D>();
    }

    // Use this for initialization
    

	// Use this for initialization
	void Start () {
        if (!isServer) return;
        isPickedUp = false;
	}

    void OnPickedUp(bool pickupStatus) {
        isPickedUp = pickupStatus;
        if (pickupStatus){
            coll.enabled = false;
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            Color col = sr.color;
            col.a = 0.4f;
            sr.color = col;
        }
        else {
            coll.enabled = true;
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            Color col = sr.color;
            col.a = 1f;
            sr.color = col;
        }
    }
}
