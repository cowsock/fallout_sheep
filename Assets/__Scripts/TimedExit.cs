﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedExit : MonoBehaviour {

    public float delay = 3f;

	// Use this for initialization
	void Start () {
        Invoke("Quit", delay);	
	}

    void Quit() {
        Application.Quit();
    }
}
