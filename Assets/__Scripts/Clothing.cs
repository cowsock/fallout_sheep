﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clothing : MonoBehaviour { // no need to be NetworkBehaviour - just need to be Instantiated in ClientRpc call

    public Vector2 trajectory;
    public float fadeDuration = 3f;

    public float degreesPerSecond;

    float startTime;
    public SpriteRenderer spriteRenderer;

    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	// Use this for initialization
	void Start () {
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        // need to fade out gradually, and then be destroyed
        float u = (Time.time - startTime) / fadeDuration;
        if (u >= 1f) {
            Destroy(gameObject);
        }
        float opacity = 1f - u;
        Color col = spriteRenderer.color;
        col.a = opacity;
        spriteRenderer.color = col;

        // also should move
        transform.Rotate(0, 0, Time.deltaTime * degreesPerSecond);
        transform.Translate(trajectory * Time.deltaTime);
    }

    
}
