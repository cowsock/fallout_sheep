﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomLobbyPlayer : NetworkLobbyPlayer {

    public NetworkLobbyPlayer lobbyPlayer;
    public LobbyUI uiRef;
    public int playerNo;
    const int min_players_c = 2;

    void Awake()
    {
        // DontDestroyOnLoad(transform.gameObject);
        lobbyPlayer = GetComponent<NetworkLobbyPlayer>();
        uiRef = GetComponent<LobbyUI>();
    }
    /*void Start() {
        //readyToBegin = true;
        //CustomLobbyPlayer[] players = FindObjectsOfType<CustomLobbyPlayer>();
        // if (players.Length != min_players_c) return;
        
        NetworkLobbyManager netLobMan = NetworkLobbyManager.singleton.GetComponent<NetworkLobbyManager>();

        transform.SetParent(netLobMan.transform);
        if ((isServer && isLocalPlayer)|| (!isServer && !isLocalPlayer)){
            netLobMan.lobbySlots[0] = this; 
            slot = 0;
            playerNo = 0;
            uiRef.InitHostUI();
        }
        else if ((!isServer && isLocalPlayer) || (isServer && !isLocalPlayer)){
            netLobMan.lobbySlots[1] = this;
            slot = (byte)1;
            playerNo = 1;
            uiRef.InitClientUI();
        }
        if (isLocalPlayer && !isServer)
        {
            GameSetup.S.localPlayer = gameObject;
            SendNotReadyToBeginMessage();
        }


    }*/


    void Start()
    {
        //readyToBegin = true;

        NetworkLobbyPlayer[] players = FindObjectsOfType<NetworkLobbyPlayer>();
        if (players.Length != min_players_c) return;
        NetworkLobbyManager netLobMan = NetworkLobbyManager.singleton.GetComponent<NetworkLobbyManager>();
        if (players[0].netId.Value < players[1].netId.Value)
        {
            netLobMan.lobbySlots[0] = players[0];
            netLobMan.lobbySlots[1] = players[1];
            players[0].slot = (byte)0;
            players[1].slot = (byte)1;
        }
        else
        {
            netLobMan.lobbySlots[0] = players[1];
            netLobMan.lobbySlots[1] = players[0];
            players[0].slot = (byte)1;
            players[1].slot = (byte)0;
        }
        if (isLocalPlayer)
            GameSetup.S.localPlayer = gameObject;
        if (netLobMan.lobbySlots[0] == lobbyPlayer)
            playerNo = 0;
        else
            playerNo = 1;
    }




}
