using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Facing_e // I should probably only have one sort of directional enum
{
    right,
    down,
    left,
    up
}

enum Direction_e {
    north,
    east,
    south,
    west,
    northeast,
    southeast,
    southwest,
    northwest
}

class Utility {
    static public Vector2 Dir2Vec(Direction_e dir) {
        switch (dir) {
            case Direction_e.north:
                return Vector2.up;
                break;
            case Direction_e.northeast:
                return Vector2.one;
                break;
            case Direction_e.east:
                return Vector2.right;
                break;
            case Direction_e.southeast:
                return new Vector2(1f, -1f);
                break;
            case Direction_e.south:
                return Vector2.down;
                break;
            case Direction_e.southwest:
                return -Vector2.one;
                break;
            case Direction_e.west:
                return Vector2.left;
                break;
            case Direction_e.northwest:
                return new Vector2(-1f, 1f);
                break;
            default:
                return Vector2.zero;
        }
    }

    static public float Dir2Angle(Direction_e dir) {
        // IN DEGREES
        switch (dir) {
            case Direction_e.north:
                return 90f;
                break;
            case Direction_e.northeast:
                return 45f;
                break;
            case Direction_e.east:
                return 0f;
                break;
            case Direction_e.southeast:
                return 315f;
                break;
            case Direction_e.south:
                return 270f;
                break;
            case Direction_e.southwest:
                return 225f;
                break;
            case Direction_e.west:
                return 180f;
                break;
            case Direction_e.northwest:
                return 135f;
                break;
            default:
                Debug.Assert(false);
                return 0f;
        }
    }

    static public Direction_e RandomDir() {
        return (Direction_e)Random.Range(0, 3);
    }

    static public Direction_e RandomDirCardinal() {
        return (Direction_e)Random.Range(0, 7);
    }

    static public Vector2 NearestDir(Vector2 vec) {
        // returns a vector 'rounded' to the nearest cardinal direction
        if (vec == Vector2.zero) return vec;
        float absX = Mathf.Abs(vec.x);
        float absY = Mathf.Abs(vec.y);
        if (absX > absY){
            return new Vector2(Mathf.Sign(vec.x), 0);
        }
        else if (absY > absX){
            return new Vector2(0, Mathf.Sign(vec.y));
        }
        else { // abs are the same, preserve sign of inputs, but normalize to 1 or -1
            return new Vector2(Mathf.Sign(vec.x), Mathf.Sign(vec.y));
        }
    }

    static public Vector2 NearestCardinal(Vector2 vec)
    {
        // returns a vector 'rounded' to the nearest cardinal direction
        if (vec == Vector2.zero) return vec;
        if (Mathf.Abs(vec.x) >= Mathf.Abs(vec.y)){
            return new Vector2(Mathf.Sign(vec.x), 0);
        }
        else{
            return new Vector2(0, Mathf.Sign(vec.y));
        }
    }

    // crappy version of Box-Mueller transform
    public static float QuickGauss(float mu, float sigma)
    {
        float u1 = 1f - Random.value;
        float u2 = 1f - Random.value;
        float norm = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) *
             Mathf.Sin(2.0f * Mathf.PI * u2);
        return mu + sigma * norm;
    }


}