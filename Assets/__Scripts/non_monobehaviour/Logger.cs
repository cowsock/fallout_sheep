﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public enum Log_Event_e {
    game_start,
    level_start,
    level_end,
    level_failed,
    level_timeout,
    fence_pickup,
    fence_setdown,
    shear_sheep,
    follow_initiated,
    player_sick,
    player_well,
    wolf_chase_begin,
    wolf_eat_sheep,
    wolf_scared_away,
    sheep_eat,
    sheep_sick,
    sheep_irradiated,
    sheep_died,
    game_end,
    interrupt,
    error
}

public class Logger  { // might as well just write text files
    static Logger s;
    static public Logger S {
        get {
            if (s == null)
                s = new Logger();
            return s;
        }
    }
    StreamWriter writer;

    Logger() {
        int log_number = Get_Log_No();
        writer = new StreamWriter(Application.persistentDataPath + "/Log_" + log_number.ToString() + ".txt");
    }

    ~Logger() { // just to be sure we write anything remaining regardless of how application quits
        writer.Flush();
        writer.Close();
    }

    int Get_Log_No() {
        if (!File.Exists(Application.persistentDataPath + "/Log_No.txt"))
        {
            FileStream logNoFile = new FileStream(Application.persistentDataPath + "/Log_No.txt", FileMode.CreateNew, FileAccess.Write);
            logNoFile.WriteByte((byte)0);
            logNoFile.Close();
            return 0;
        }
        else
        {
            FileStream logNoFile = new FileStream(Application.persistentDataPath + "/Log_No.txt", FileMode.Open, FileAccess.Read);
            int logNo = logNoFile.ReadByte();
            logNoFile.Close();
            logNoFile = new FileStream(Application.persistentDataPath + "/Log_No.txt", FileMode.Open, FileAccess.Write);
            logNoFile.WriteByte((byte)++logNo);
            logNoFile.Close();
            return logNo;
        }
    }

    public void Flush() {
        writer.Flush();
    }

    public void Log(Log_Event_e eventType) {
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType));
    }

    public void Log(Log_Event_e eventType, Vector2 player1Pos, Vector2 player2Pos) {
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType), player1Pos.ToString(), player2Pos.ToString());
        // get timestamp for right now. 

        // record the event type

        // get positions of both players (Vector2)


        // optional mushroom type + radioactivity for sheep_eat event
        
        // optional position of relevant 3rd thing (Vector2)

        // optional position of relevant 4th thing (Vector2) e.g. wolf (3rd thing) ate sheep (4th thing)

       
    }

    public void Log(Log_Event_e eventType, Vector2 player1Pos, Vector2 player2Pos, Vector2 nonPlayerSubj){
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType), player1Pos.ToString(), player2Pos.ToString(), nonPlayerSubj.ToString());
    }

    public void Log(Log_Event_e eventType, Vector2 player1Pos, Vector2 player2Pos, Vector2 nonPlayerSubj, Vector2 nonPlayerObj){
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType), player1Pos.ToString(), player2Pos.ToString(), nonPlayerSubj.ToString(), nonPlayerObj.ToString());
    }

    public void Log(Log_Event_e eventType, Vector2 player1Pos, Vector2 player2Pos, Vector2 nonPlayerSubj, MushroomType_e mushroomType, bool mushroomRadioactive){
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType), player1Pos.ToString(), player2Pos.ToString(), nonPlayerSubj.ToString(), mushroomType.ToString(), mushroomRadioactive.ToString());
    }

    public void Log(Log_Event_e eventType, Vector2 player1Pos, Vector2 player2Pos, Vector2 sheepPos, int wool_amount) {
        WriteToLog(Time.realtimeSinceStartup.ToString(), EventTypeToString(eventType), player1Pos.ToString(), player2Pos.ToString(), sheepPos.ToString(), wool_amount.ToString());
    }

    void WriteToLog(params string[] entries) {
       writer.WriteLine(string.Join("\t", entries));
    }

    string EventTypeToString(Log_Event_e eventType) {
        switch (eventType) {
            case Log_Event_e.game_start:
                return "GameStart";
            case Log_Event_e.level_start:
                return "LevelStart";
            case Log_Event_e.fence_pickup:
                return "FencePickup";
            case Log_Event_e.fence_setdown:
                return "FenceSetdown";
            case Log_Event_e.shear_sheep:
                return "ShearSheep";
            case Log_Event_e.follow_initiated:
                return "FollowStarted";
            case Log_Event_e.player_sick:
                return "PlayerSick";
            case Log_Event_e.player_well:
                return "PlayerWell";
            case Log_Event_e.wolf_chase_begin:
                return "WolfChaseStart";
            case Log_Event_e.wolf_eat_sheep:
                return "WolfAteSheep";
            case Log_Event_e.wolf_scared_away:
                return "WolfScaredAway";
            case Log_Event_e.sheep_eat:
                return "SheepAteFood";
            case Log_Event_e.sheep_sick:
                return "SheepSick";
            case Log_Event_e.sheep_irradiated:
                return "SheepIrradiated";
            case Log_Event_e.sheep_died:
                return "SheepDied";
            case Log_Event_e.level_end:
                return "LevelEnd";
            case Log_Event_e.level_failed:
                return "LevelFailed";
            case Log_Event_e.level_timeout:
                return "LevelTimeout";
            case Log_Event_e.game_end:
                return "GameEnd";
            case Log_Event_e.interrupt:
                return "Interrput";
            case Log_Event_e.error:
                return "Error";
            default:
                return "Error";
        }
    }
	
}
