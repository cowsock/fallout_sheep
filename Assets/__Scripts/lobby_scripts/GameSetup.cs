﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameSetup : MonoBehaviour {
    public static GameSetup S;
    public GameObject localPlayer;

    public bool clientConnected = false;

    MyNetworkManager netManager;

    void Awake() {
        S = this;
        netManager = MyNetworkManager.singleton.GetComponent<MyNetworkManager>();
    }

    // Use this for initialization
    void Start() {
        if (!PlayerPrefs.HasKey("isHost"))
        {
            return; // await GUI type setup
        }
        else {
            netManager.GetComponent<NetworkManagerHUD>().showGUI = false;
            //MyNetworkManager.singleton.GetComponent<NetworkLobbyManager>().showLobbyGUI = false;
            if (PlayerPrefs.GetInt("isHost") == 1) {
                netManager.StartHost(); // this shouldn't fail
                //InitHostUI();
            }
            else { // setup client
                // get parameters to connect to host. addr and so forth
                string addr = PlayerPrefs.GetString("hostAddr");
                int port = PlayerPrefs.GetInt("port");
                netManager.networkAddress = addr;
                netManager.networkPort = port;

                StartCoroutine(AttemptClientConnect());
                //MyNetworkManager.singleton.StartClient();
            }
            
        }
    }


    IEnumerator AttemptClientConnect() {
        for (;;)
        {
            if (!clientConnected && !netManager.isNetworkActive)
            {
                netManager.StartClient();
                yield return new WaitForSeconds(2f);
            }
            else break;
        }
    }

 

    void Update() { //***BE SURE TO REMOVE THIS
        // for playerpref debug use
        if (Input.GetKey(KeyCode.F5))
            PlayerPrefs.DeleteAll();
    } 

   
}
