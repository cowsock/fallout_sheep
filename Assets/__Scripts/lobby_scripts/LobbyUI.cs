﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

 
 public class LobbyUI : NetworkBehaviour
{

    Text mismatch_txt;
    Button submitButton;
    InputField teamname_input;
    Button player1ReadyButton;
    Button player2ReadyButton;
    Image player1Check;
    Image player2Check;
    Text teamName;

    Transform canvas;

    CustomLobbyPlayer lobbyPlayer;

    [SyncVar]
    public string playerName;

    [SyncVar]
    public bool isReady = false;
 
    CustomLobbyPlayer[] lobbyPlayers;

 
     // get refs to GUI components here.
     void Awake()
    {
        lobbyPlayer = GetComponent<CustomLobbyPlayer>();
        canvas = GameObject.Find("Canvas").transform;
        teamname_input = canvas.Find("InputField").GetComponent<InputField>();
        submitButton = canvas.Find("Submit_Name_Button").GetComponent<Button>();
        player1ReadyButton = canvas.Find("LobbySlots/P1_Ready_Button").GetComponent<Button>();
        player2ReadyButton = canvas.Find("LobbySlots/P2_Ready_Button").GetComponent<Button>();
        player1Check = canvas.Find("LobbySlots/P1_Check").GetComponent<Image>();
        player2Check = canvas.Find("LobbySlots/P2_Check").GetComponent<Image>();
        mismatch_txt = canvas.Find("names_dont_match_text").GetComponent<Text>();
        teamName = canvas.Find("TeamName").GetComponent<Text>();
        lobbyPlayers = new CustomLobbyPlayer[2];
    }

    // Use this for initialization
    void Start()
    {
        if (!isLocalPlayer) return;
        lobbyPlayer.SendNotReadyToBeginMessage();
        if (isServer)
            InitHostUI();
        else if (isClient)
            InitClientUI();

    }


    [ServerCallback]
    void Update(){
        lobbyPlayers = FindObjectsOfType<CustomLobbyPlayer>();
                if (lobbyPlayers.Length < 2) return;
                if (lobbyPlayers[0].uiRef.isReady && lobbyPlayers[1].uiRef.isReady){
                     CmdSendReadySignal();
                }
            }

    public void InitHostUI()
    {
        if (!isLocalPlayer) return;
        teamname_input.onValueChanged.AddListener(delegate { OnTeamnameUpdate(); });
        //teamname_input.onValueChanged.AddListener(delegate { OnChangePlayer1Name(); });
        submitButton.onClick.AddListener(delegate { OnPlayerNameButton(); });
        player1ReadyButton.onClick.AddListener(delegate { CmdOnPlayerReady(lobbyPlayer.playerNo); });
        player2ReadyButton.interactable = false;
        canvas.Find("YouAre1").gameObject.SetActive(true);
    }

    public void InitClientUI()
    {
        if (!isLocalPlayer) return;
        teamname_input.onValueChanged.AddListener(delegate { OnTeamnameUpdate(); });
        //teamname_input.onValueChanged.AddListener(delegate { OnChangePlayer2Name(); });
        submitButton.onClick.AddListener(delegate { OnPlayerNameButton(); });
        player2ReadyButton.onClick.AddListener(delegate { CmdOnPlayerReady(lobbyPlayer.playerNo); });
        player1ReadyButton.interactable = false;
        canvas.Find("YouAre2").gameObject.SetActive(true);
    }
 

 
     [Command]
    public void CmdUpdateTeamName(string newname)
    {
        playerName = newname;
    }


    [Command]
    public void CmdOnPlayerReady(int playerNo)
    {
        if (playerNo == 0)
            RpcOnPlayer1Ready();
        else
            RpcOnPlayer2Ready();
            
        isReady = true;
    }

    [ClientRpc]
    public void RpcOnPlayer1Ready()
    {
        if (lobbyPlayer.playerNo == 0)
        {
            //GetComponent<CustomLobbyPlayer>().SendReadyToBeginMessage();
            player1ReadyButton.interactable = false;
        }
        player1Check.gameObject.SetActive(true);
    }

    [ClientRpc]
    public void RpcOnPlayer2Ready()
    {
        if (lobbyPlayer.playerNo == 1)
        { // player 1 in the zero-indexed sense
          //GetComponent<CustomLobbyPlayer>().SendReadyToBeginMessage();
            player2ReadyButton.interactable = false;
        }
        player2Check.gameObject.SetActive(true);
    }
 
    [Command]
    void CmdSendReadySignal()
    {
        RpcSendReadySignal();
    }

     [ClientRpc]
    void RpcSendReadySignal()
    {
        lobbyPlayer.SendReadyToBeginMessage();
    }


    public void OnTeamnameUpdate()
    {
        if (!isLocalPlayer) return;
        // print(teamname_input.text);
        CmdUpdateTeamName(teamname_input.text);
        //  GameSetup.S.localPlayer.GetComponent<CustomLobbyPlayer>().CmdUpdateTeamname(teamname_input.text);
    }

    public void OnPlayerNameButton()
    {
        // need to check if those strings are equal   --- various other interactions if not.
        //print("onplayernamebutton");
        LobbyUI[] lobbyPlayers = FindObjectsOfType<LobbyUI>();
        if (lobbyPlayers.Length < 2) return;
        bool names_equal = (lobbyPlayers[0].playerName == lobbyPlayers[1].playerName);
        if (lobbyPlayers[0].playerName != "" && lobbyPlayers[1].playerName != "" &&
            names_equal)
        {
            // print("strings are equal");
            CmdTeamnameSet();
            //GameSetup.S.localPlayer.GetComponent<CustomLobbyPlayer>().CmdSendTeamName(); // problem is that this needs to happen on both sides!
        }
        else if (!names_equal)
        {
            StartCoroutine(DisplayErrMsg("Namen stimmen nicht überein!")); // "names aren't equal"
        }
    }

    [Command]
    public void CmdTeamnameSet()
    {
        RpcTeamnameSet();
    }


    [ClientRpc]
    void RpcTeamnameSet()
    { // made available network-wise
      //print("OnReady callback");
        teamName.text = "Team: " + teamname_input.text;
        teamName.gameObject.SetActive(true);
        teamname_input.interactable = false;
        submitButton.interactable = false;
        MyNetworkManager.singleton.GetComponent<MyNetworkManager>().teamName = teamname_input.text;
        //prompt.text = "For audio synchronization, only plug in headphones AFTER game begins";
        //prompt.text = "Bitte schließen Sie Ihre Kopfhörer erst an, NACHDEM das Spiel gestartet hat, sodass die Audiosynchronisation problemlos erfolgen kann.";
        if (isServer)
        {
            player1ReadyButton.interactable = true;
        }
        else if (isClient)
        {
            player2ReadyButton.interactable = true;
        }
    }

    IEnumerator DisplayErrMsg(string msg)
    {
        mismatch_txt.text = msg;
        mismatch_txt.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        mismatch_txt.gameObject.SetActive(false);
    }

}
