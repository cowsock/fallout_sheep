﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets._2D;
//using UnityEngine.AI;

[NetworkSettings(channel = 0) ]
public class Player : NetworkBehaviour {
    public float speed;


    public bool actionButtonPrev = false;
    public bool actionButton = false;
    public bool actionAvailable = false;
    [SyncVar] public bool holdingFence = false;
    public bool shearing = false;

    bool canMove = true;
    bool isSick = false;

    public GameObject fenceRef;
    public GameObject sheepRef;

    public int wool_collected = 0;

    Vector2 input;
    Vector2 cachedFenceOffset;

    // for assigning player number:

    public int playerNo;

    Movement mv;

    Animator anim;

    // NavMeshAgent agent;

    [HideInInspector]
    public Sprite sickSprite;

    AudioSource audSource;

    void Awake() {

        mv = GetComponent<Movement>();
        anim = GetComponent<Animator>();
        GetComponent<NetworkAnimator>().animator = anim;
        audSource = GetComponent<AudioSource>();
        
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        GameObject cam = Instantiate(GameState.S.camPrefab);
        Vector3 camPos = new Vector3(transform.position.x, transform.position.y, -1f);
        cam.transform.position = camPos;
        cam.GetComponent<Camera2DFollow>().enabled = true;
        cam.GetComponent<Camera2DFollow>().target = transform;
        gameObject.AddComponent<AudioListener>();

    }



    // Use this for initialization
    void Start() {
        actionButton = false;
        if (isServer) //syncvar init
            holdingFence = false;

        /*CustomLobbyPlayer[] lobbyPlayers = FindObjectsOfType<CustomLobbyPlayer>();
        Debug.Assert(lobbyPlayers.Length == 2);
        foreach (CustomLobbyPlayer clp in lobbyPlayers) {
            
        }*/
        
        if (isServer && isLocalPlayer)
        {
            playerNo = 0; 
        }
        else if (isServer && !isLocalPlayer)
        {
            playerNo = 1; 
        }
        else if (!isServer && isLocalPlayer)
        {
            playerNo = 1; 
        }
        else if (!isServer && !isLocalPlayer) {
            playerNo = 0;
        
        }

        // variable initialization section
        if (playerNo == 0)
        {
            GetComponent<SpriteRenderer>().sprite = GameState.S.player0Sprite;
            
            transform.position = GameState.S.spawnPoint0.position;
            sickSprite = GameState.S.player0SpriteSick;
            if (isLocalPlayer)
            {
                GameObject UI = Instantiate(GameState.S.engineerUI);
                UI.transform.SetParent(transform);
            }
            GameState.S.player1Transform = transform;
        }
        else {
            GetComponent<SpriteRenderer>().sprite = GameState.S.player1Sprite;
            transform.position = GameState.S.spawnPoint1.position;
            sickSprite = GameState.S.player1SpriteSick;
            if (isLocalPlayer)
            {
                GameObject UI = Instantiate(GameState.S.shepherdUI);
                UI.transform.SetParent(transform);
            }
            GameState.S.player2Transform = transform;
        }
        
    }

    // Update is called once per frame
    [Client]
    void Update () {
        if (!isLocalPlayer) return;
        // use to get input from user
        actionButtonPrev = actionButton; // ensure one button press per action
        actionButton = Input.GetKey(KeyCode.Space);
        if (actionButton && !actionButtonPrev)
            actionAvailable = true;

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        
    }

    [Client]
    void FixedUpdate(){
        if (!isLocalPlayer) return;
        if (!canMove) return;
        float dt = Time.fixedDeltaTime;
        Vector2 pos = transform.position;
        pos.x += input.x * dt * speed;
        pos.y += input.y * dt * speed;
        // EXPERIMENTAL MOVEMENT
        if (input.x != 0f || input.y != 0f)
            //agent.destination = pos;
            mv.Move(pos);
       // transform.position = pos;
        

        // actions
        if (playerNo == 0 && holdingFence)
            CmdMoveFence(pos);
        if (!actionAvailable) {
            return;
        }
        actionAvailable = false; // for next time

        if (playerNo == 0)
        {
            if (holdingFence)
            {
                CmdSetDownFence();
                //if (!isServer) // client needs to change things back on server and locally (that or I need a re-design)
                //SetDownFence();     
            }
            else
            {
                //PickupFence();
                CmdPickUpFence();
            }
        }
        if (sheepRef && !shearing) {
            Sheep sheep = sheepRef.GetComponent<Sheep>();
            if (!sheep.isShearable) {
                return;
            }
            int wool_amount = (int)sheep.wool_amount;
            if (sheep.radioactive){
                wool_amount = 0;
                StartCoroutine(Sickness());
            }
            else if (wool_amount == 2) {// for fluffy
                ++wool_amount; // bonus amount for fluffy sheep
            }
            //shearing = true;
            StartCoroutine(Shear(wool_amount));       
        }
    }

    [Command]
    void CmdMoveFence(Vector2 pos) {
        if (fenceRef == null) return;
        fenceRef.transform.position = pos + cachedFenceOffset;
    }

    [Command]
    void CmdPickUpFence() {
        if (fenceRef == null) return;
        holdingFence = true;
        fenceRef.GetComponent<Fence>().isPickedUp = true;
        cachedFenceOffset = fenceRef.transform.position - transform.position;
        Logger.S.Log(Log_Event_e.fence_pickup, GameState.S.player1Transform.position, GameState.S.player2Transform.position);
    }

   

    [Command]
    void CmdSetDownFence() {
        if (fenceRef == null) return;
        holdingFence = false;
        fenceRef.GetComponent<Fence>().isPickedUp = false;
        fenceRef = null;
        Logger.S.Log(Log_Event_e.fence_setdown, GameState.S.player1Transform.position, GameState.S.player2Transform.position);
    }


    [Command]
    void CmdStartShear() {
        RpcStartShear();
        sheepRef.GetComponent<Sheep>().StartShearing();
    }

    [Command]
    void CmdEndShear() {
        RpcEndShear();
    }

    [ClientRpc]
    void RpcStartShear() {
        audSource.Play(); // no need to check isPlaying due to action restrictions
    }

    [ClientRpc]
    void RpcEndShear() {
        audSource.Stop();
    }

    [Client]
    IEnumerator Shear(int woolAmount) {
        shearing = true;
        canMove = false;
        CmdStartShear();
        yield return new WaitForSeconds(GameState.shearTime_c);
        CmdEndShear();
        wool_collected += woolAmount;
        CmdNotifyWoolCollected(woolAmount, sheepRef);
        canMove = true;
        shearing = false;
    }

    [Command]
    void CmdNotifyWoolCollected(int woolAmount, GameObject shearedSheep) {
        Logger.S.Log(Log_Event_e.shear_sheep, GameState.S.player1Transform.position, GameState.S.player2Transform.position, shearedSheep.transform.position, woolAmount);
        if (shearedSheep.GetComponent<Sheep>().radioactive) {
            Logger.S.Log(Log_Event_e.player_sick, GameState.S.player1Transform.position, GameState.S.player2Transform.position);
        }
        GameState.S.wool_collected += woolAmount; // is a synchvar, so update needs to happen server-side
    }

    [Client]
    IEnumerator Sickness() {
        if (isSick) yield break;
        isSick = true;
        yield return new WaitForSeconds(2f);
        float prevSpeed = speed;
        speed /= 2.5f;
        anim.SetBool("Sick", true);
        yield return new WaitForSeconds(15f);
        anim.SetBool("Sick", false);
        speed = prevSpeed;
        isSick = false;
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if (isServer && coll.gameObject.tag == "Fence")
        {
            if (!holdingFence) // don't want to override reference
            {
                fenceRef = coll.gameObject;
            }
        }
        else if (coll.gameObject.tag == "Sheep") {
            if (!shearing)
                sheepRef = coll.gameObject;
        }
    }

    void OnTriggerStay2D(Collider2D coll){
        if (isServer && coll.gameObject.tag == "Fence")
        {
            if (!holdingFence) // don't want to override reference
                fenceRef = coll.gameObject;
        }
        else if (coll.gameObject.tag == "Sheep") {
            if (!shearing)
                sheepRef = coll.gameObject;
        }
        
    } 

    void OnTriggerExit2D(Collider2D coll) {
        if (isServer && coll.tag == "Fence" && !holdingFence && fenceRef == coll.gameObject)
        {
            fenceRef = null;
        }
        else if (coll.tag == "Sheep" && !shearing && sheepRef == coll.gameObject)
            sheepRef = null;
    }



}
