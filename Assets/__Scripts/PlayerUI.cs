﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    Text woolText;

    Text sheepText;

    Text levelCompleteText;
    Text teamnameText;
    Text levelTimer;

    CanvasGroup helpOverlay;

    public Player playerRef;

    string totalSheep;
    string goalWoolAmount;

    float currentTime;

    void Awake() {
        woolText = transform.FindChild("Wool_amount").GetComponent<Text>(); // in the future, lookups can be done via index number
        sheepText = transform.FindChild("Sheep_remaining").GetComponent<Text>();
        levelCompleteText = transform.FindChild("Goal_text").GetComponent<Text>();
        teamnameText = transform.FindChild("TeamName").GetComponent<Text>();
        levelTimer = transform.FindChild("Timer").GetComponent<Text>();
        helpOverlay = transform.FindChild("Help_diagram").GetComponent<CanvasGroup>();
    }

    void Start() {
        GameState.S.player_ui = this; // slightly unorthodox initialization
        playerRef = GetComponentInParent<Player>();
        totalSheep = Sheep.sheepLs.Count.ToString();
        goalWoolAmount = GameState.S.goal_wool_amount.ToString();
        MyNetworkManager manager = MyNetworkManager.singleton.GetComponent<MyNetworkManager>();
        if (manager != null)
            teamnameText.text = manager.teamName;
        currentTime = 0f;
    }
	
	
	void FixedUpdate () {
        if (Input.GetKey(KeyCode.H))
        {
            if (helpOverlay.alpha < 1) {
                helpOverlay.alpha = (1 - 0.02f) * helpOverlay.alpha + 0.02f;
            }
        }
        else {
            if (helpOverlay.alpha > 0)
            {
                helpOverlay.alpha = (1 - 0.02f) * helpOverlay.alpha - 0.02f;
            }
        }
        //woolText.text = "Wool: " + playerRef.wool_collected.ToString() + " (me) " + GameState.S.wool_collected.ToString() + "/" + goalWoolAmount + " (total)";
        woolText.text = "Wolle:\n" + GameState.S.wool_collected.ToString() + "/" + goalWoolAmount;
        sheepText.text = "Schafe:\n" + Sheep.sheepLs.Count.ToString() + "/" + totalSheep;

        currentTime += Time.deltaTime;
        float timeLeft = GameState.S.time_limit_seconds - currentTime;
        if (timeLeft < 0) timeLeft = 0;
        string mins = Mathf.Floor(timeLeft / 60).ToString("0");
        string secs = Mathf.Floor(timeLeft % 60).ToString("00");
        levelTimer.text = mins + ":" + secs;
        if (timeLeft < 60) levelTimer.color = Color.red;

    }

    public void ActivateGoalText(int level_no) {
        levelCompleteText.gameObject.SetActive(true);
        //levelCompleteText.text = "Nice going!" + "\nYou collected " + (GameState.S.wool_collected).ToString() + " bundles of wool";
        levelCompleteText.text = "Gute Arbeit!" + "\nSie haben " + (GameState.S.wool_collected).ToString() + " Wollbündel gesammelt.";

    }

    public void ActivateFailureText() {
        levelCompleteText.gameObject.SetActive(true);
        //levelCompleteText.text = "Oh no!\nYou've lost all of your sheep! Restarting level...";
        levelCompleteText.text = "Oh je! Sie haben alle Ihre Schafe verloren! Starte Level erneut...";
    }

    public void ActivateTimeoutText() {
        levelCompleteText.gameObject.SetActive(true);
        //levelCompleteText.text = "Oh no!\nYou've run out of time! Starting next level...";
        levelCompleteText.text = "Oh nein! Die Zeit ist abgelaufen! Beginne nächstes Level...";
    }
}
