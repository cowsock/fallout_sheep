﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Wolf : NetworkBehaviour
{

    // Behaviours:
    //   navigate toward, try and eat a sheep
    //   then escape for a while

    //   If BOTH players are close enough, will run away (faster than them)
    //   (maybe just growl if only one player is nearby)

    // pathfinding type code can probs be shared between here and sheep

    public float speed;
    public float maxTargetDistance; // obvs needs to be positive

    public bool isHungry;

    bool _isFrightened = false;
    public bool isFrightened {
        get {
            return _isFrightened;
        }         
    }

    public Transform[] denLocations;
    int currentDen = 0;

    Direction_e direction;

    public Vector2 curVelocity;

    Collider2D[] playerColliders = new Collider2D[2];
    LayerMask playerLayerMask; // just the player

    LayerMask planningCollisions; // for rerouting around obstacles
    LayerMask targetCollisions; // used for raycast to find a sheep to try and eat, but is obstacle sensitive

    bool routineActive = false;
    string moveRoutine;
    bool runningAway = false;

    Sheep target;

    Movement mv;

    Animator anim;

    AudioSource aud;
    public AudioClip growl;
    public AudioClip howl;


    void Awake(){
        playerColliders = new Collider2D[2];
        playerLayerMask = 1 << LayerMask.NameToLayer("Player");
        planningCollisions = 1 << LayerMask.NameToLayer("Fence");
        planningCollisions += 1 << LayerMask.NameToLayer("Default");
        targetCollisions = planningCollisions + 1 << LayerMask.NameToLayer("Sheep");
        mv = GetComponent<Movement>();
        anim = GetComponent<Animator>();
        aud = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start(){
        isHungry = true;
        routineActive = false;
    }

    

    Sheep GetTarget() {
        // first try to see if there's a sheep in line of sight
        RaycastHit2D rayHit = Physics2D.BoxCast(transform.position,
                new Vector2(0.5f, 0.5f), Vector2.Angle(Vector2.zero, Utility.Dir2Vec(direction)),
                    Utility.Dir2Vec(direction), 3f, targetCollisions);
        if (rayHit && rayHit.collider.tag == "Sheep") {
            return rayHit.collider.GetComponent<Sheep>();
        }
        // otherwise pick nearest target within maxTargetDistance away
        float nearest = maxTargetDistance;
        target = null;
        foreach (Sheep s in Sheep.sheepLs) {
            float dist = (s.transform.position - transform.position).magnitude;
            // could also check if it's possible to reach them
            if (dist < nearest) {
                nearest = dist;
                target = s;
            }
        }
        return target;
    }

    [Server]
    IEnumerator Chase() {
        if (routineActive) yield break;
        Logger.S.Log(Log_Event_e.wolf_chase_begin, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
        routineActive = true;
        RpcHowl();
        float startTime = Time.time;
        while (target != null && Time.time - startTime < 25f)  
        {
            
            Vector2 heading = target.transform.position - transform.position;
            curVelocity = mv.MoveToTarget(heading, curVelocity, target.transform.position, speed);
            CheckDirectionChange();
            yield return null;
        }
        routineActive = false;
    }

    IEnumerator RunAway() {
        if (routineActive) yield break;
        routineActive = true;
        float startTime = Time.time;
        runningAway = true;
        while (Time.time - startTime < 25f) {
            // go toward den
            Vector2 heading = denLocations[currentDen].position - transform.position;
            /*curVelocity = (1 - 0.25f) * curVelocity + (0.25f * heading);
            curVelocity = curVelocity.normalized * speed;
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);*/
            curVelocity = mv.MoveToTarget(heading, curVelocity, denLocations[currentDen].position, speed * 1.5f);
            CheckDirectionChange();
            yield return null;
        }
        routineActive = false;
        runningAway = false;
        if (isFrightened) {
            _isFrightened = false;
            anim.SetBool("Frightened", false);
        }
    }

    IEnumerator Chill() {
        if (routineActive) yield break;
        routineActive = true;
        NextDen();
        Vector2 direction = denLocations[currentDen].position - transform.position;
        float startTime = Time.time;
        while (Time.time - startTime < 6f) {
            /*curVelocity = (1 - 0.25f) * curVelocity + (0.25f * direction); 
            curVelocity = curVelocity.normalized * (speed/2);
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime); */
            curVelocity = mv.MoveToTarget(direction, curVelocity, direction * 2, speed);
            CheckDirectionChange();
            yield return null;
        }
        routineActive = false;
    }

    IEnumerator Digestion() {
        isHungry = false;
        yield return new WaitForSeconds(25f);
        isHungry = true;
    }

    [Server]
    void Eat(Sheep sheep) {
        Logger.S.Log(Log_Event_e.wolf_eat_sheep, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
        sheep.Kill();
        target = null;
        StartCoroutine(Digestion());
        StartCoroutine("RunAway");
        moveRoutine = "RunAway";
    }

    [ServerCallback]
    void FixedUpdate() {
        if (runningAway) return;
        int results = Physics2D.OverlapCircleNonAlloc(transform.position, 1.5f, playerColliders, playerLayerMask);
        if (results == 2)
        {
            // maybe include a yelping sound or whatever
            if (routineActive && moveRoutine == "RunAway") return; // just in case
            StopCoroutine(moveRoutine);
            routineActive = false;
            _isFrightened = true;
            anim.SetBool("Frightened", true);
            StartCoroutine("RunAway");
            moveRoutine = "RunAway";
            Logger.S.Log(Log_Event_e.wolf_scared_away, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
            return;
        }
        else if (results == 1) { // indicating that there's something happening
            RpcGrowl();
        }

        if (routineActive) return;

        if (target == null && isHungry) {
            target = GetTarget(); // could still be null
            if (target) {
                StartCoroutine("Chase");
                moveRoutine = "Chase";
            }
        }
        if (target == null || !isHungry) {
            StartCoroutine("Chill");
            moveRoutine = "Chill";
        }

        
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D collision){
        if (collision.tag == "Sheep") {
            if (isHungry)
                Eat(collision.gameObject.GetComponent<Sheep>());
        }
    }


    void CheckDirectionChange(){
        if (!isServer) return; // because this is an animation issue
        if (curVelocity == Vector2.zero)
            anim.SetBool("Moving", false);
        else
            anim.SetBool("Moving", true);
        if (Mathf.Abs(curVelocity.x) > Mathf.Abs(curVelocity.y))
        {
            if (curVelocity.x > 0)
            {
                direction = Direction_e.east;
                anim.SetInteger("Direction", 0);
            }
            else
            {
                direction = Direction_e.west;
                anim.SetInteger("Direction", 2);
            }
        }
        else
        {
            if (curVelocity.y > 0)
            {
                direction = Direction_e.north;
                anim.SetInteger("Direction", 3);
            }
            else
            {
                direction = Direction_e.south;
                anim.SetInteger("Direction", 1);
            }
        }
    }

    void NextDen() {
        if (currentDen + 1 >= denLocations.Length)
        {
            currentDen = 0;
        }
        else
            ++currentDen;
    }

    [ClientRpc]
    void RpcHowl() {
        if (aud.isPlaying) return;
        aud.clip = howl;
        aud.Play();
    }

    [ClientRpc]
    void RpcGrowl() {
        if (aud.isPlaying) return;
        aud.clip = growl;
        aud.Play();
    }

}
