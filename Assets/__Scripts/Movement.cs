﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour {


    public LayerMask collisionLayers;

    public float raycastDist;

    float radius; 

    RaycastHit2D[] hits2D = new RaycastHit2D[2]; // pre-allocated buffer

    int rotateDirection = 1;
    Vector2 rerouteHeading = Vector2.zero;
    bool rerouteActive = false;
    bool rerouteAvailable = true;


    void Awake() {
        // set up things relating to grabbing the bounds of our collider
        CircleCollider2D circColl = GetComponent<CircleCollider2D>();
        if (circColl != null)
            radius = circColl.radius;
        else {
            // gonna get a fake radius
            Collider2D coll = GetComponent<Collider2D>();
            radius = coll.bounds.max.x - coll.bounds.center.x;
        }
            

        
        radius += raycastDist;
    } 

    // Use this for initialization
    void Start () {
        StartCoroutine(RerouteTimer());
	}

    IEnumerator RerouteTimer() {
        for (;;) {
            yield return new WaitForSeconds(1.2f);
            rerouteAvailable = true;
        }
    }

  

    public void Move(Vector2 moveAttempt) {
        Vector2 movePos = GetValidPosition(moveAttempt, 20f, 40f, 60f, 80f);
        transform.position = movePos;
    }

    public Vector2 MoveToTarget(Vector2 moveAttempt, Vector2 curVelocity, Vector2 target, float speed) {
        // need to avoid obstacles in the way of target. Will probably be super inefficient
        // but whatever, optimize later if needed. (to not have to calculate this every fixed update frame)
        Vector3 heading = (Vector3)target - transform.position;
        if (rerouteAvailable)
        {
            RaycastHit2D rayHit = Physics2D.BoxCast(transform.position, new Vector2(0.03f, 0.01f), 
                Vector2.Angle(Vector2.zero, heading), heading, 0.6f, collisionLayers);
            //RaycastHit2D rayHit = Physics2D.Raycast(transform.position, heading, 1f, collisionLayers); 
            Debug.DrawRay(transform.position, heading, Color.red, 1f);
            rerouteAvailable = false;
            if (rayHit)
            {
                rerouteActive = true;
                Vector2 normal = Utility.NearestCardinal(rayHit.normal);
                Debug.DrawRay(transform.position, normal, Color.magenta, 1f);
                if (normal == Vector2.left)
                {
                    rerouteHeading = Quaternion.Euler(0, 0, rotateDirection * 80f) * heading;
                    //heading = new Vector2(rayHit.point.x, rayHit.point.y + 1f) * heading.magnitude;
                }
                else if (normal == Vector2.right)
                    rerouteHeading = Quaternion.Euler(0, 0, rotateDirection * -80f) * heading;
                else if (normal == Vector2.up)
                    rerouteHeading = Quaternion.Euler(0, 0, rotateDirection * 80f) * heading;
                else
                    rerouteHeading = Quaternion.Euler(0, 0, rotateDirection * -80f) * heading;

                if (Random.value < 0.05f) // to avoid getting stuck 1/20
                    rotateDirection *= -1; // flip it (but only sometimes)

                Debug.DrawRay(transform.position, rerouteHeading, Color.yellow, 1f);
            }
            else
                rerouteActive = false;
 
          
        }
        if (rerouteActive)
            heading = rerouteHeading;
        else
            heading = moveAttempt;

        curVelocity = (1 - 0.25f) * curVelocity + (0.25f * (Vector2)heading);
        curVelocity = curVelocity.normalized * speed;
        Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
        return curVelocity;
    }

    // Some of the stuff below based on https://github.com/adamscoble/TDCharacterController2D 

    Vector2 GetValidPosition(Vector2 movementTarget, params float[] testAngles) {
        Vector2 targetDir = ((Vector3)movementTarget - transform.position).normalized * radius;

        //Debug.DrawLine(movementTarget, movementTarget + targetDir, Color.cyan);

        movementTarget += GetDirectionAdjustment(movementTarget, targetDir);

        // adjust by +/- rotation of all test angles
        for (int i = 0; i < testAngles.Length; ++i) {
            movementTarget += GetDirectionAdjustment(movementTarget, 
                Quaternion.Euler(0, 0, testAngles[i]) * targetDir);
            movementTarget += GetDirectionAdjustment(movementTarget,
                Quaternion.Euler(0, 0, -testAngles[i]) * targetDir);

            //Debug.DrawLine(movementTarget, movementTarget + 
            //    (Vector2)(Quaternion.Euler(0, 0, testAngles[i]) * targetDir), Color.cyan);
           // Debug.DrawLine(movementTarget, movementTarget + 
            //    (Vector2)(Quaternion.Euler(0, 0, -testAngles[i]) * targetDir), Color.cyan);
        }

        return movementTarget;
    }

    Vector2 GetDirectionAdjustment(Vector2 targetPos, Vector2 direction) {
        Vector2 adjustment = Vector2.zero;

        int numHits = Physics2D.RaycastNonAlloc(targetPos, direction, hits2D, raycastDist, collisionLayers);
        RaycastHit2D rayHit;

        if (numHits == 0 || (numHits == 1 && hits2D[0].fraction < 0.6f))
            return adjustment;
        else if (numHits == 1 && hits2D[0].fraction > 0.6f)
            rayHit = hits2D[0];
        else
            rayHit = hits2D[1];

        return rayHit.normal.normalized * ((1f - rayHit.fraction) * radius);
    }
}
