﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class GameState : NetworkBehaviour {

    static public GameState S;
    public Sprite player0Sprite;
    public Sprite player0SpriteSick;
    public Sprite player1Sprite;
    public Sprite player1SpriteSick;
    public int clothing_types_c = 5;
    public int clothing_colors_c = 5;
    public Sprite[] clothingSprites;

    public GameObject shepherdUI;
    public GameObject engineerUI;
    public GameObject camPrefab;

    public Transform spawnPoint0;
    public Transform spawnPoint1;

    public const int num_players_c = 2;


    public int level_num;

    public const float shearTime_c = 1.5f;

    public int goal_wool_amount;

    public float time_limit_seconds = 120f;

    public string next_level;

    [SyncVar] public int wool_collected;

    //[SyncVar] public int sick_sheep;
    //[SyncVar] public int lost_sheep; 

    public PlayerUI player_ui;

    public Transform player1Transform;
    public Transform player2Transform;

    bool transition = false; // used to prevent level end stuff from being called a bunch of times

    float startTime;


    void Awake(){
        S = this;
        //DontDestroyOnLoad(S);
        player1Transform = spawnPoint0; // defaults so that if a player hasn't spawned yet, there will be valid values for the logger
        player2Transform = spawnPoint1;
       
    }

    void Start() {
        if (!isServer) return;
        if (SceneManager.GetActiveScene().name == "Level_0") {
            Logger.S.Log(Log_Event_e.game_start, Vector2.zero, Vector2.zero);
            GetComponent<AudioSource>().Play();
        }
        Logger.S.Log(Log_Event_e.level_start, spawnPoint0.position, spawnPoint1.position);
        startTime = Time.time;
        transition = false;
    }
   

    [ServerCallback]
     void FixedUpdate(){ 
        if (transition) return; // to prevent double calls for scene change
        if (wool_collected >= goal_wool_amount) // level clear condition(s)
        {
            transition = true;
            RpcGoalText();
            Invoke("NextScene", 5f);
        }
        else if (Sheep.sheepLs.Count <= 0)
        { // losing condition(s)
            transition = true;
            RpcFailText();
            Invoke("RestartScene", 5f);
        }
        else if (Time.time - startTime > time_limit_seconds)
        {
            transition = true;
            Logger.S.Log(Log_Event_e.level_timeout);
            RpcTimeoutText();
            Invoke("NextScene", 5f);
        }
        else if (Input.GetKey(KeyCode.F1) && Input.GetKey(KeyCode.F2))
        {
            transition = true;
            NextScene();
        }
        else if (Input.GetKey(KeyCode.F3) && Input.GetKey(KeyCode.F4))
        {
            transition = true;
            RestartScene();
        }
     }

    [ClientRpc]
    void RpcGoalText() {
        player_ui.ActivateGoalText(level_num);
    }

    [ClientRpc]
    void RpcFailText() {
        player_ui.ActivateFailureText();
    }

    [ClientRpc]
    void RpcTimeoutText() {
        player_ui.ActivateTimeoutText();
    }

    [Server]
    void NextScene() {
        Logger.S.Log(Log_Event_e.level_end);
        Logger.S.Flush();
        if (next_level != "")
        {
            //NetworkServer.SetAllClientsNotReady();
            NetworkManager.singleton.ServerChangeScene(next_level);
        }
        else {
            Logger.S.Log(Log_Event_e.game_end);
        }
    }


    [Server]
    void RestartScene() {
        Logger.S.Log(Log_Event_e.level_failed);
        Logger.S.Flush();
        //NetworkServer.SetAllClientsNotReady();
        NetworkManager.singleton.ServerChangeScene(SceneManager.GetActiveScene().name);
    }
    

    
 
}
