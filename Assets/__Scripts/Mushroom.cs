﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public enum MushroomType_e {
    normal,
    poison,
    hallucinogen, 
    nutritious
}

public class Mushroom : MonoBehaviour {

    public Sprite genericSprite;

    public Sprite specialSprite;

    public MushroomType_e type;

    public bool isRadioactive;

    SpriteRenderer render;

    void Awake() {
        render = GetComponent<SpriteRenderer>();
    }

	// Use this for initialization
	void Start () {
        // check player number - initialize spriteRenderer accordingly
        NetworkIdentity id = GetComponent<NetworkIdentity>();
        if (id.isServer) {
            render.sprite = genericSprite;
        }
        else {
            render.sprite = specialSprite;
            if (isRadioactive) {
                GetComponent<AudioSource>().enabled = false;
            }
        }
	}
	
	
}
