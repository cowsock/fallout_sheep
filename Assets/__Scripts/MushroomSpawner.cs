﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MushroomSpawner : NetworkBehaviour {

    public GameObject shroomType;
    public float spawnRate; // how frequently a new mushroom will spawn
    public float spawnRadius; // radius around spawner where they are spawnable
    public int maxMushrooms; // max number that can be active

    int mushroomCount = 0;
    float timer = 0f;

    // remember to call NetworkManager.Spawn to spawn a mushroom

 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (timer < spawnRate) {
            timer += Time.deltaTime;
            return;
        }
        if (mushroomCount >= maxMushrooms) return;  //NOTE: at the moment, it could just destroy itself if at maxMushrooms
        Vector3 spawnPos = transform.position + (Vector3)Random.insideUnitCircle * spawnRadius;
        GameObject shroom = Instantiate(shroomType);
        ++mushroomCount;
        shroom.transform.position = spawnPos;
        NetworkServer.Spawn(shroom);
        timer = 0f;
		
	}
}
