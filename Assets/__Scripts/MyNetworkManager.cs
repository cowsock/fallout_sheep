﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MyNetworkManager : NetworkLobbyManager {

    /*public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId){
        GameObject player = Instantiate(playerPrefab);

        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }*/

    public string teamName;

  

    public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        base.OnLobbyClientSceneChanged(conn);
        
    }


    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        // it's entirely possible that this doesn't do anything meaningful
    {
        if (SceneManager.GetActiveScene().name.Equals(lobbyScene))
        {
            GameObject lobbyPlayer = Instantiate(lobbyPlayerPrefab.gameObject);
            NetworkServer.AddPlayerForConnection(conn, lobbyPlayer, 0);
            
        }
        else {
            GameObject player = Instantiate(playerPrefab);
            NetworkServer.AddPlayerForConnection(conn, player, 0);
        }
       

    }

   /* public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
    }*/


   /* public override void OnClientConnect(NetworkConnection conn)
    {
        ClientScene.AddPlayer(conn, 0);
    }*/


    public override void OnStartHost()
    {
        base.OnStartHost();
        if (!PlayerPrefs.HasKey("isHost"))
        {
            PlayerPrefs.SetInt("isHost", 1);
            PlayerPrefs.Save();
           // FindObjectOfType<LobbyUI>().InitHostUI();

        }

    }

    public override void OnStartClient(NetworkClient lobbyClient)
    {
        base.OnStartClient(lobbyClient);
        GameSetup.S.clientConnected = true;
        if (!PlayerPrefs.HasKey("isHost")) {
            PlayerPrefs.SetInt("isHost", 0);
            PlayerPrefs.SetString("hostAddr", networkAddress);
            PlayerPrefs.SetInt("port", networkPort);
            PlayerPrefs.Save();
           // FindObjectOfType<LobbyUI>().InitClientUI();
        }

    }

}
