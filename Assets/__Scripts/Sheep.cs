﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public enum Wool_e { // could this be nested in Sheep class?
    shaved,
    mid,
    fluffy
}


public class Sheep : NetworkBehaviour {

    const int sick_amount_rad_c = 1;
    const int sick_amount_poison_c = 1;

    static public List<Sheep> sheepLs;
    public AudioClip[] baa_Sounds;
    public AudioClip radiationSound;
    public AudioClip[] sick_sounds;

    public GameObject clothingPrefab;

    [Space]

    [SyncVar (hook = "OnChangeFace")]
    public Facing_e direction;

    [SyncVar(hook = "OnChangeWool")]
    public Wool_e wool_amount;

    [SyncVar]
    public bool isShearable = false;

    [SyncVar (hook = "OnChangeSickness")]
    public bool isSick = false;

    [SyncVar(hook = "OnDying")]
    public bool isDead = false;

    Animator anim; // reference to animator component

    public List<Sheep> neighbors; // all nearby agents
    public List<Sheep> collisionRisks; // all agents that are too close

    public float nearDist = 5f;
    public float collisionDist = 1f; // could maybe change this approach later in favor of real collisions. 

    public float speed = 0.5f;
    public float moveDuration = 2f;

    [SyncVar] public int foodEaten = 0;

    [SyncVar] public int radioactiveFoodEaten = 0;
    [SyncVar] public bool radioactive = false;
    [SyncVar] public int poisonFoodEaten = 0;

    LayerMask collisionLayers;

    Vector2 curVelocity;

    Mushroom foodRef;
    Player playerRef;

    Collider2D[] playerColliders;

    public string moveRoutine;
    bool routineActive = false;

    Wolf wolfRef;
    bool runningAway = false;


    public int disinterest = 0;
    float cachedFoodDistance = 0f;

    Movement mv;
    AudioSource baa_audio;
    AudioSource radiation_audio;
    AudioSource sick_audio;

    /*enum Sheep_State_e{
        wander,
        follow,
        forage
    }Sheep_State_e state;*/

    //bool moving;
    [Space]

    public float foodTrajectory_adjust;
    public float neighborCenterAdjust;
    public float collisionAvoidAdjust;

    public float recoveryTime = 30f;


    void Awake() {
        
        if (sheepLs == null)
        {
            sheepLs = new List<Sheep>();
        }
        sheepLs.Add(this);

        neighbors = new List<Sheep>();
        collisionRisks = new List<Sheep>();

        anim = GetComponent<Animator>();
        mv = GetComponent<Movement>();
        playerColliders = new Collider2D[GameState.num_players_c];

        radiation_audio = GetComponent<AudioSource>(); // IMPORTANT SCRIPT EXECUTION ORDERING HERE
        baa_audio = AddAudio(baa_Sounds[Random.Range(0, baa_Sounds.Length)], 0.55f);
        sick_audio = AddAudio(sick_sounds[Random.Range(0, sick_sounds.Length)], 0.4f);

    }

    // Use this for initialization
    void Start() {
        foodEaten = 0;

        collisionLayers = 1 << LayerMask.NameToLayer("Fence");
        collisionLayers += 1 << LayerMask.NameToLayer("Default");

        curVelocity = Random.insideUnitCircle * speed; // NETWORKING ALERT -> RANDOMIZED VALUE (could sync seed)
        
        // moving = false;
        if (!isServer) return;
        //state = Sheep_State_e.wander;
        // direction = Facing_e.right;
        anim.SetInteger("Direction", (int)direction);
        anim.SetInteger("Wool", (int)wool_amount);
        // wool_amount = Wool_e.shaved;
        if ((int)wool_amount > 0) {
            isShearable = true;
        }
        CheckRadioactivity();

    }

    [ServerCallback]
    void FixedUpdate(){
        // Sickness counter
        if (Time.timeSinceLevelLoad % 60 == recoveryTime) {
            if (poisonFoodEaten > 0)
            {
                --poisonFoodEaten;
            }
            if (radioactiveFoodEaten > 0)
            {
                --radioactiveFoodEaten;
            }
            if (poisonFoodEaten < sick_amount_poison_c &&
               radioactiveFoodEaten < sick_amount_rad_c)
            {
                isSick = false;
                radioactive = false;
                radiation_audio.enabled = false;
            }
        }
        // checking for wolves
        if (!runningAway)
        {
            Collider2D coll = Physics2D.OverlapCircle(transform.position, 2f, 1 << LayerMask.NameToLayer("Wolf"));
            if (coll)
            {
                // get that ref
                wolfRef = coll.GetComponent<Wolf>();
                StopCoroutine(moveRoutine);
                routineActive = false; // since we broke out
                moveRoutine = "RunAway";
                StartCoroutine("RunAway");

            }
            // see if we'll play a sheep noise
            if (!baa_audio.isPlaying && Utility.QuickGauss(-3.5f, 1f) > 0f)
            {
                RpcBaa();
            }
            else if (!sick_audio.isPlaying && Utility.QuickGauss(-3f, 1f) > 0f)
            {
                RpcBleh();
            }
        }

        
        if (routineActive) return;

        // following check
        playerRef = GetPlayerTarget();  // is there a player nearby?
        if (!runningAway && playerRef != null)
        {
            anim.SetBool("Following", true);
            foodRef = GetFoodTarget();
            moveRoutine = "FollowPlayer";
            StartCoroutine("FollowPlayer");
        }
        else // food check
        {
            anim.SetBool("Following", false);
            //yield return new WaitForSeconds(Utility.QuickGauss(2f, 1f));
            if (!foodRef || disinterest > 3)
            {
                foodRef = GetFoodTarget();
                disinterest = 0; // if they keep searching for something, eventually they get bored.
                if (foodRef)
                    cachedFoodDistance = (foodRef.transform.position - transform.position).magnitude;
            }
            if (foodRef)
            {
                float newDistance = (foodRef.transform.position - transform.position).magnitude;
                if (newDistance > cachedFoodDistance) // we're farther away
                    ++disinterest;
                else
                    cachedFoodDistance = newDistance;
                moveRoutine = "SeekFood";
                StartCoroutine("SeekFood");
            }
            else
            {
                moveRoutine = "RandomMove";
                StartCoroutine("RandomMove");
            }
        }
    }

    [ClientRpc]
    void RpcBaa() {
        if (!baa_audio.isPlaying && !isDead)
            baa_audio.Play();
    }

    [ClientRpc]
    void RpcBleh() {
        if (!sick_audio.isPlaying && !isDead && isSick) {
            sick_audio.Play();
        }
    }

    //******this could just be a function in Movement.cs?
    void MovementSetup(Vector2 destination, float multiplier) {
        curVelocity = (1 - 0.25f) * curVelocity + (0.25f * destination);
        curVelocity = curVelocity.normalized * speed * multiplier;
        mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
        CheckDirectionChange();
    }



    [Server]
    IEnumerator SeekFood() {
        if (routineActive || foodRef == null) yield break;
        Vector2 heading = foodRef.transform.position - transform.position;
        Vector2 foodPos = (Vector2)foodRef.transform.position;
        routineActive = true;
        
        yield return new WaitForSeconds(Utility.QuickGauss(1f, 1f));

        float startTime = Time.time;
        while (Time.time - startTime < moveDuration)
        {
            curVelocity = mv.MoveToTarget(heading, GetVelocity(curVelocity), foodPos, speed * 1.12f);
            CheckDirectionChange();
            yield return null;
        }
        routineActive = false;
    }

    [Server]
    IEnumerator FollowPlayer() { // assumes playerRef isn't null!
        if (routineActive || playerRef == null) yield break;
        routineActive = true;
        Vector2 dir = playerRef.transform.position - transform.position;
        yield return new WaitForSeconds(Utility.QuickGauss(1f, 0.5f));
        Logger.S.Log(Log_Event_e.follow_initiated, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
        float startTime = Time.time;
        
        // what if we could ignore this or adjust it if there's no immediate path to the player

        while (Time.time - startTime < moveDuration) {
            curVelocity = mv.MoveToTarget(dir, GetVelocity(curVelocity), dir, speed * 1.6f);
            CheckDirectionChange();
            yield return null;
        }
        routineActive = false;
    }

    [Server]
    IEnumerator RandomMove() {
        if (routineActive) yield break;
        routineActive = true;
        yield return new WaitForSeconds(Utility.QuickGauss(2f, 1f));
        float startTime = Time.time;
        Vector2 nextVelocity = Random.insideUnitCircle * (speed );
        while (Time.time - startTime < moveDuration)
        {
            curVelocity = mv.MoveToTarget(nextVelocity, GetVelocity(curVelocity), nextVelocity * 5f, speed);
            CheckDirectionChange();
            //MovementSetup(nextVelocity, 1f);
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        //StartCoroutine("CheckIn");
        //moveRoutine = "CheckIn";
        routineActive = false;
    }

    

    [Server]
    IEnumerator RunAway() {
        if (routineActive) yield break;
        routineActive = true;
        Debug.Assert(wolfRef != null);
        runningAway = true;
        anim.SetBool("Following", false);
        anim.SetBool("Frightened", true);

        for (int i = 0; i < 8; ++i) { // just to re-check every now and again
            Vector2 heading = transform.position - wolfRef.transform.position;
            Vector2 direction = heading / heading.magnitude;
            if (i % 2 == 0){ // just to not baa so obnoxiously often
                RpcBaa();
            }
            Debug.DrawRay(transform.position, direction, Color.white, 1f);
            if (wolfRef.isFrightened) {
                break; // no need to keep running if wolf is going away
            }

            float startTime = Time.time;

            while (Time.time - startTime < 1f)
            {
                /*curVelocity = (1 - 0.25f) * curVelocity + (0.25f * direction);
                curVelocity = curVelocity.normalized * speed * 2;
                mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime); */
                curVelocity = mv.MoveToTarget(direction, GetVelocity(curVelocity), direction * 2, speed * 2);
                CheckDirectionChange();
                yield return null;
            }
        }
        routineActive = false;
        anim.SetBool("Frightened", false);
        runningAway = false;
        wolfRef = null;
    }

    void OnChangeFace(Facing_e new_direction) {
        direction = new_direction;
        anim.SetInteger("Direction", (int)new_direction);
    }

    void OnChangeWool(Wool_e new_amt) {
        wool_amount = new_amt;
        anim.SetInteger("Wool", (int)new_amt);
    }

    void OnChangeSickness(bool new_status) {
        isSick = new_status;
        anim.SetBool("Sick", new_status);
    }

    void OnDying(bool new_val) {
        Debug.Assert(new_val);
        isDead = new_val;
        anim.SetTrigger("Dead");
    }

    [ServerCallback]
    void OnTriggerStay2D(Collider2D coll){
        if (coll.tag == "Mushroom") {
            if (foodRef && foodRef.gameObject == coll.gameObject){
                Eat();
            }
        }
    }


    [Server]
    void Eat() {
        Mushroom mushroomRef = foodRef.GetComponent<Mushroom>();
        Logger.S.Log(Log_Event_e.sheep_eat, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position, mushroomRef.type, mushroomRef.isRadioactive);
        if (mushroomRef.isRadioactive)
        {
            ++radioactiveFoodEaten;
            ++foodEaten;
            if (radioactiveFoodEaten >= sick_amount_rad_c && !isSick)
            {
                Logger.S.Log(Log_Event_e.sheep_irradiated, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
                StartCoroutine(ShowSymptoms());
            }
            if (radioactiveFoodEaten > 4)
            {
                Kill();
            }
        }
        if (mushroomRef.type == MushroomType_e.poison)
        {
            ++poisonFoodEaten;
            if (poisonFoodEaten >= sick_amount_poison_c && !isSick)
            {
                Logger.S.Log(Log_Event_e.sheep_sick, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
                StartCoroutine(ShowSymptoms());
            }
            if (poisonFoodEaten > 2)
            {

                Kill();
            }
        }
        else if (mushroomRef.type == MushroomType_e.nutritious) {
            foodEaten += 4;
        }
        else
        {
            ++foodEaten;
        }
        CheckRadioactivity();
            
        NetworkServer.Destroy(foodRef.gameObject);
        Destroy(foodRef.gameObject);
        foodRef = null;
        if (foodEaten > 3)
        {
            wool_amount = Wool_e.fluffy;
            isShearable = true;
        }
        else if (foodEaten > 1)
        {
            wool_amount = Wool_e.mid;
            isShearable = true;
        }
        else {
            wool_amount = Wool_e.shaved;
            isShearable = false;
        }
        disinterest = 0;
    }

    void CheckRadioactivity(){
        if (radioactiveFoodEaten > 1 && isServer) {
            // this only works because player 1 is also the server
            radiation_audio.enabled = true;
            radiation_audio.clip = radiationSound;
            radiation_audio.Play();
            radioactive = true;
        }
    }


    [Server]
    IEnumerator ShowSymptoms() {
        yield return new WaitForSeconds(2f);
        isSick = true;
        // could possibly slow the sheep down or whatever
        float oldSpeed = speed;
        speed /= 2;
        while (isSick) {
            yield return new WaitForSeconds(2f);
        }
        speed = oldSpeed;
    }

    [Server]
    public void StartShearing() {
        StopCoroutine(moveRoutine);
        anim.SetBool("Following", false);
        isShearable = false; // immediately send synchvar message that this sheep cannot be sheared again
        routineActive = true;
        StartCoroutine(Shear());
    }

    [Server]
    IEnumerator Shear() {
        yield return new WaitForSeconds(GameState.shearTime_c);
        EndShearing();
    }

    [Server]
    void EndShearing() {
        RpcClothingParticles(wool_amount);
        wool_amount = Wool_e.shaved; 
        foodEaten = 0;
        routineActive = false;
    }

    [ClientRpc]
    void RpcClothingParticles(Wool_e amt) {
        // spawn particle(s)
        if (amt == Wool_e.shaved || radioactive) {
            return;
        }
        Clothing particle1 = GetNewClothingParticle(false);
        if (amt == Wool_e.fluffy) {
            Clothing particle2 = GetNewClothingParticle(true);
            Clothing particle3 = GetNewClothingParticle(true);
            particle2.trajectory = Quaternion.Euler(0, 0, -45f) * particle1.trajectory;
            particle3.trajectory = Quaternion.Euler(0, 0, 45f) * particle1.trajectory;
        }
    }

    Clothing GetNewClothingParticle(bool isFancy) {
        GameObject particle = Instantiate(clothingPrefab);
        Clothing cloth = particle.GetComponent<Clothing>();
        int index = 0;
        if (isFancy)
        {
            index = Random.Range(3, 5) + (5 * Random.Range(0, 5));
        }
        else {
            index = Random.Range(0, 3) + (5 * Random.Range(0, 5));
        }
        // somehow need to get the right sprite
        cloth.spriteRenderer.sprite = GameState.S.clothingSprites[index];


        particle.transform.position = transform.position;
        cloth.trajectory = Random.insideUnitCircle;

        return cloth;
    }

    [Server]
    Vector2 GetVelocity(Vector2 moveAttempt, float interpolation_amt = 0.25f){

        GetNeighbors();
        if (neighbors.Count == 0) return curVelocity;

        Vector2 newVelocity = curVelocity;

        // flocking behavior (could be parameterized)
        Vector2 neighborCenterOffset = GetAveragePosition(neighbors) - transform.position;
        newVelocity += neighborCenterOffset * neighborCenterAdjust;

        // collision avoidance 
        Vector3 dist;
        if (collisionRisks.Count > 0){
            Vector3 collisionAveragePos = GetAveragePosition(collisionRisks);
            dist = collisionAveragePos - transform.position;
            newVelocity += (Vector2)dist * collisionAvoidAdjust; // collision avoidance amount
        }

        return (1 - interpolation_amt) * moveAttempt + (interpolation_amt * newVelocity);
        // yet another interpolation parameter
    }

    // returns Agents near enough to ag to be considered neighbors
    void GetNeighbors(){
        Vector3 delta;
        float dist;
        neighbors.Clear();
        collisionRisks.Clear();

        foreach (Sheep s in sheepLs){
            if (s == this) continue;
            delta = s.transform.position - transform.position;
            dist = delta.magnitude;
            if (dist < nearDist){
                neighbors.Add(s);
            }
            if (dist < collisionDist){
                collisionRisks.Add(s);
            }
        }   
    }

    [Server]
    Mushroom GetFoodTarget() {
        Vector2 castDir = Vector2.zero;
        switch (direction) {
            case Facing_e.right:
                castDir = Vector2.right;
                break;
            case Facing_e.down:
                castDir = Vector2.down;
                break;
            case Facing_e.left:
                castDir = Vector2.left;
                break;
            case Facing_e.up:
                castDir = Vector2.up;
                break;
        }
        Collider2D circCast = Physics2D.OverlapCircle(transform.position, 0.75f, 1 << LayerMask.NameToLayer("Mushroom"));
        if (circCast)
        {
            return circCast.gameObject.GetComponent<Mushroom>();
        }
        else {
            RaycastHit2D rayHit = Physics2D.BoxCast(transform.position,
                new Vector2(0.5f, 0.5f), Vector2.Angle(Vector2.zero, castDir),
                    castDir, 3f, 1 << LayerMask.NameToLayer("Mushroom"));
            if (rayHit)
                return rayHit.transform.gameObject.GetComponent<Mushroom>();
            Debug.DrawLine(transform.position, transform.position + (Vector3)castDir * 3f, Color.red, 1.5f);
        }
        
        return null;
    }

    Player GetPlayerTarget() {
        Physics2D.OverlapCircleNonAlloc(transform.position, 0.8f, playerColliders,  1 << LayerMask.NameToLayer("Player"));
        for (int i = 0; i < GameState.num_players_c; ++i) {
            if (playerColliders[i] != null) {
                Player playerRef = playerColliders[i].GetComponent<Player>();
                if (playerRef.playerNo == 1){ // only follow player 1
                    return playerRef;
                }
            }
        }  
        return null;
    }

    public Vector3 GetAveragePosition(List<Sheep> someSheep){
        Vector3 sum = Vector2.zero;
        foreach (Sheep s in someSheep){
            sum += s.transform.position;
        }
        Vector3 center = sum / someSheep.Count;
        return center;
    }

    public Vector2 GetAverageVelocity(List<Sheep> someSheep){
        Vector2 sum = Vector3.zero;
        foreach (Sheep s in someSheep){
            sum += s.curVelocity;
        }
        Vector2 avg = sum / someSheep.Count;
        return avg;
    }

    void CheckDirectionChange() {
        if (!isServer) return;
        if (Mathf.Abs(curVelocity.x) > Mathf.Abs(curVelocity.y))
        {
            if (curVelocity.x > 0)
                direction = Facing_e.right;
            else
                direction = Facing_e.left;
        }
        else
        {
            if (curVelocity.y > 0)
                direction = Facing_e.up;
            else
                direction = Facing_e.down;
        }
    }


    public override void OnNetworkDestroy()
    {
        OnDestroy();
    }

    void OnDestroy(){
        if (sheepLs.Contains(this)) // because it could be removed earlier
            sheepLs.Remove(this);
    }

    [Server]
    public void Kill() { // interface for something to merc a sheep -> don't call Destroy() or NetworkServer.Destroy() directly
        Logger.S.Log(Log_Event_e.sheep_died, GameState.S.player1Transform.position, GameState.S.player2Transform.position, transform.position);
        sheepLs.Remove(this);
        isDead = true;
        StopAllCoroutines();
        RpcDeathBlink();
        Invoke("Die", 3f);
    }

    [ClientRpc]
    void RpcDeathBlink() {
        StartCoroutine(DeathBlink());
    }

    IEnumerator DeathBlink() { // typical videogame having sprite blink in and out before disappearing
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        for (;;) {
            sr.enabled = !sr.enabled;
            yield return new WaitForSeconds(0.15f);
        }
    }

    void Die() {
        NetworkServer.Destroy(gameObject);
        Destroy(gameObject);
    }


    AudioSource AddAudio(AudioClip clip, float vol) {
        AudioSource new_audio = gameObject.AddComponent<AudioSource>();
        new_audio.clip = clip;
        new_audio.volume = vol;
        new_audio.playOnAwake = false;
        new_audio.rolloffMode = AudioRolloffMode.Linear;
        new_audio.spatialBlend = 1.0f;
        new_audio.maxDistance = 5f;
        return new_audio;
    }

}
