﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum Facing_e
{
    right,
    down,
    left,
    up
}

public enum Wool_e { // could this be nested in Sheep class?
    shaved,
    mid,
    fluffy
}

public class Sheep : NetworkBehaviour {

    static public List<Sheep> sheepLs;

    [SyncVar (hook = "OnChangeFace")]
    public Facing_e direction;

    [SyncVar(hook = "OnChangeWool")]
    public Wool_e wool_amount;

    [SyncVar (hook = "OnChangeSickness")]
    public bool isSick = false;

    Animator anim; // reference to animator component

    public List<Sheep> neighbors; // all nearby agents
    public List<Sheep> collisionRisks; // all agents that are too close

    public float nearDist = 5f;
    public float collisionDist = 1f; // could maybe change this approach later in favor of real collisions. 

    public float speed = 0.5f;
    public float moveDuration = 2f;

    [SyncVar] public int foodEaten = 0;

    [SyncVar] public int radioactiveFoodEaten = 0;
    [SyncVar] public bool radioactive = false;
    [SyncVar] public int poisonFoodEaten = 0;

    LayerMask collisionLayers;

    Vector2 curVelocity;

    Mushroom foodRef;
    Player playerRef;

    public int disinterest = 0;

    Movement mv;

    //bool moving;
    [Space]

    public float waitDuration;
    public float foodTrajectory_adjust;
    public float neighborCenterAdjust;
    public float collisionAvoidAdjust;

    void Awake() {
        if (sheepLs == null)
        {
            sheepLs = new List<Sheep>();
        }
        sheepLs.Add(this);

        neighbors = new List<Sheep>();
        collisionRisks = new List<Sheep>();

        anim = GetComponent<Animator>();
        //rb = GetComponent<Rigidbody2D>();
        mv = GetComponent<Movement>();
    }

    // Use this for initialization
    void Start() {
        foodEaten = 0;

        collisionLayers = 1 << LayerMask.NameToLayer("Fence");
        collisionLayers += 1 << LayerMask.NameToLayer("Default");

        curVelocity = Random.insideUnitCircle * speed; // NETWORKING ALERT -> RANDOMIZED VALUE (could sync seed)
        direction = Facing_e.right;
        wool_amount = Wool_e.shaved;
        // moving = false;
        if (!isServer) return;


        StartCoroutine(Delay());
    }

    [Server]
    IEnumerator Delay()
    {
      
        yield return new WaitForSeconds(GameState.QuickGauss(2f, 1f));
        playerRef = GetPlayerTarget();  // is there a player nearby?
        if (playerRef != null)
        {
            StartCoroutine("FollowPlayer");
        }
        else
        {
            if (!foodRef || disinterest > 3)
            {
                foodRef = GetFoodTarget();
                disinterest = 0; // if they keep searching for something, eventually they get bored.
            }
            if (foodRef)
            {
                StartCoroutine("SeekFood");
                ++disinterest;
            }
            else
            {
                StartCoroutine("RandomMove");
            }
        }
    }


    [Server]
    IEnumerator SeekFood() {
        float startTime = Time.time;
        //CheckDirectionChange();

        Vector2 dist = foodRef.transform.position - transform.position;
        Vector2 newVelocity = dist * foodTrajectory_adjust;
        // so what if we had a chance to adjust this if we don't have a direct path to the food?
        /*RaycastHit2D rayHit = Physics2D.Raycast(transform.position, newVelocity, dist.magnitude, collisionLayers);
        if (rayHit) {
            // do somethin or another, shit fam.
        }*/

        while (Time.time - startTime < moveDuration) {
            curVelocity = (1 - 0.25f) * curVelocity + (0.25f * newVelocity);
            curVelocity = curVelocity.normalized * speed * 1.12f;
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
            //rb.MovePosition(transform.position + (Vector3)curVelocity * Time.deltaTime);
            CheckDirectionChange();
            yield return null;
        }

        StartCoroutine("CheckIn");
    }

    [Server]
    IEnumerator FollowPlayer() { // assumes playerRef isn't null!
        float startTime = Time.time;

        Vector2 dir = playerRef.transform.position - transform.position;
        // what if we could ignore this or adjust it if there's no immediate path to the player

        while (Time.time - startTime < moveDuration/2) {
            curVelocity = (1 - 0.25f) * curVelocity + (0.25f * dir);
            curVelocity = curVelocity.normalized * speed * 1.3f;
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
            CheckDirectionChange();
            yield return null;
        }
        StartCoroutine("Delay");
    }

    [Server]
    IEnumerator RandomMove() {
        float startTime = Time.time;
        Vector2 nextVelocity = Random.insideUnitCircle * (speed );
        while (Time.time - startTime < moveDuration)
        {
            curVelocity = (1 - 0.25f) * curVelocity + (0.25f * nextVelocity);
            curVelocity = curVelocity.normalized * speed;
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
            CheckDirectionChange();
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        StartCoroutine("CheckIn");
    }

    [Server]
    IEnumerator CheckIn() {
        float startTime = Time.time;
        while (Time.time - startTime < moveDuration)
        {
            curVelocity = (1 - 0.25f) * curVelocity + (0.25f * GetVelocity()); // lerp amount could be parameterized
            curVelocity = curVelocity.normalized * speed;
            mv.Move(transform.position + (Vector3)curVelocity * Time.deltaTime);
            CheckDirectionChange();
            yield return null;
        }
        StartCoroutine("Delay");
    }

    void OnChangeFace(Facing_e new_direction) {
        direction = new_direction;
        anim.SetInteger("Direction", (int)new_direction);
    }

    void OnChangeWool(Wool_e new_amt) {
        wool_amount = new_amt;
        anim.SetInteger("Wool", (int)new_amt);
    }

    void OnChangeSickness(bool new_status) {
        isSick = new_status;
        anim.SetBool("Sick", new_status);
    }

    [ServerCallback]
    void OnTriggerStay2D(Collider2D coll){
        if (coll.tag == "Mushroom" && foodRef && foodRef.gameObject == coll.gameObject){
            Eat();
        }
    }


    [Server]
    void Eat() {
        Mushroom mushroomRef = foodRef.GetComponent<Mushroom>();
        if (mushroomRef.type == MushroomType_e.radioactive)
        {
            ++radioactiveFoodEaten;
            ++foodEaten;
        }
        else if (mushroomRef.type == MushroomType_e.poison)
        {
            ++poisonFoodEaten;
            if (!isSick)
                StartCoroutine(ShowSymptoms()); // crucially not called with a string - can't be cancelled
            if (poisonFoodEaten > 2) {
                // this effect would need to be represented better graphically.
                // perhaps by having them look sickly, and then get better over time.
                sheepLs.Remove(this); // will be updated on client via OnNetworkDestroy callback
                NetworkServer.Destroy(gameObject);
                Destroy(gameObject);
            }
        }
        else
        {
            ++foodEaten;
        }
        if (radioactiveFoodEaten > 1 && isServer) {
            // this only works because player 1 is also the server
            AudioSource audio = GetComponent<AudioSource>();
            audio.enabled = true;
            audio.Play();
            radioactive = true;
        }
            
        NetworkServer.Destroy(foodRef.gameObject);
        Destroy(foodRef.gameObject);
        foodRef = null;
        if (foodEaten > 4)
        {
            wool_amount = Wool_e.fluffy;
        }
        else if (foodEaten > 2)
        {
            wool_amount = Wool_e.mid;
        }
        else {
            wool_amount = Wool_e.shaved;
        }
        disinterest = 0;
    }

    [Server]
    IEnumerator ShowSymptoms() {
        yield return new WaitForSeconds(10f);
        isSick = true;
        // could possibly slow the sheep down or whatever
        float oldSpeed = speed;
        speed /= 2;
        yield return new WaitForSeconds(120f);
        isSick = false;
        --poisonFoodEaten;
        speed = oldSpeed;
    }

    [Server]
    public void StartShearing() {
        StopAllCoroutines();
    }

    [Server]
    public void EndShearing() {
        wool_amount = Wool_e.shaved;
        foodEaten = 0;
       // RpcResetWool();
        StartCoroutine("Delay");
    }

    // needs to be called on clients too!
   /* [ClientRpc]
    void RpcResetWool() {
        wool_amount = Wool_e.shaved;
        foodEaten = 0;
    }*/

    [Server]
    Vector2 GetVelocity(){

        GetNeighbors();
        if (neighbors.Count == 0) return curVelocity;

        Vector2 newVelocity = curVelocity;

        // flocking behavior (could be parameterized)
        Vector2 neighborCenterOffset = GetAveragePosition(neighbors) - transform.position;
        newVelocity += neighborCenterOffset * neighborCenterAdjust;

        // collision avoidance 
        Vector3 dist;
        if (collisionRisks.Count > 0){
            Vector3 collisionAveragePos = GetAveragePosition(collisionRisks);
            dist = collisionAveragePos - transform.position;
            newVelocity += (Vector2)dist * collisionAvoidAdjust; // collision avoidance amount
        }

        return newVelocity;	
    }

    // returns Agents near enough to ag to be considered neighbors
    void GetNeighbors(){
        Vector3 delta;
        float dist;
        neighbors.Clear();
        collisionRisks.Clear();

        foreach (Sheep s in sheepLs){
            if (s == this) continue;
            delta = s.transform.position - transform.position;
            dist = delta.magnitude;
            if (dist < nearDist){
                neighbors.Add(s);
            }
            if (dist < collisionDist){
                collisionRisks.Add(s);
            }
        }   
    }

    Mushroom GetFoodTarget() {
        Vector2 castDir = Vector2.zero;
        switch (direction) {
            case Facing_e.right:
                castDir = Vector2.right;
                break;
            case Facing_e.down:
                castDir = Vector2.down;
                break;
            case Facing_e.left:
                castDir = Vector2.left;
                break;
            case Facing_e.up:
                castDir = Vector2.up;
                break;
        }

        RaycastHit2D rayHit = Physics2D.BoxCast(transform.position, 
            new Vector2(0.5f, 0.5f), Vector2.Angle(Vector2.zero, castDir), 
                castDir, 2f, 1 << LayerMask.NameToLayer("Mushroom"));
        Debug.DrawLine(transform.position, transform.position + (Vector3)castDir * 2f, Color.red, 1.5f);
        if (rayHit)
            return rayHit.transform.gameObject.GetComponent<Mushroom>();
        return null;
    }

    Player GetPlayerTarget() {
        Collider2D coll = Physics2D.OverlapCircle(transform.position, 0.75f, 1 << LayerMask.NameToLayer("Player"));
        if (coll != null)
            return coll.GetComponent<Player>();
        return null;
    }

    public Vector3 GetAveragePosition(List<Sheep> someSheep){
        Vector3 sum = Vector2.zero;
        foreach (Sheep s in someSheep){
            sum += s.transform.position;
        }
        Vector3 center = sum / someSheep.Count;
        return center;
    }

    public Vector2 GetAverageVelocity(List<Sheep> someSheep){
        Vector2 sum = Vector3.zero;
        foreach (Sheep s in someSheep){
            sum += s.curVelocity;
        }
        Vector2 avg = sum / someSheep.Count;
        return avg;
    }

    void CheckDirectionChange() {
        if (Mathf.Abs(curVelocity.x) > Mathf.Abs(curVelocity.y))
        {
            if (curVelocity.x > 0)
                direction = Facing_e.right;
            else
                direction = Facing_e.left;
        }
        else
        {
            if (curVelocity.y > 0)
                direction = Facing_e.up;
            else
                direction = Facing_e.down;
        }
    }

    public override void OnNetworkDestroy()
    {
        OnDestroy();
    }

    private void OnDestroy()
    {
       sheepLs.Remove(this);
    }


    
}
