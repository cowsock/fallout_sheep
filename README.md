# README #

![hearts](screenshots/hearts.gif) This project was for my masters thesis at Uni Stuttgart, which I worked on for the better part of 2017. ![hearts](screenshots/hearts.gif)

It is a psycholinguistic 'experiment' -- less in the sense of hypothesis testing and more in an exploratory / abductive reasoning sense. ![bubbles-gif](screenshots/sick_bubbles.gif)

The goal was to create a task that could elicit different turn-taking cues/strategies employed by German speakers in situations of incomplete knowledge. The two-player cooperative computer game in this repo manipulates information available to either participant in a systematic (and dynamic) fashion.

The full text is available in this repo.

### Screenshots ###

(Sorry that they are obnoxiously sized!)

In each level, the players are tasked with shearing sheep to acquire wool (represented by winter clothing 'particles').

![shear](screenshots/shear.png)

The players have differing views of certain hazards (e.g. poisonous mushrooms, which only player 2 can distinguish). 

![player1View](screenshots/poison_shrooms.png)

![player2View](screenshots/poison_shrooms_2.png)

The players each have an ability to either help guide the sheep toward safe food or away from dangerous food.

![fence](screenshots/fence.png)

![follow](screenshots/follow.png)


### What is this repository for? ###

Originally for personal / archival purposes, now as part of a portfolio of my work. 

### How do I get set up? ###

#### The quick way: ####

Open the project in the Unity inspector, create a build of it, run both the built executable and the game within the inspector. In this case, there will be no need to enter an IP address for the client. Instead, just click the button for client without entering anything into the text box.

#### Alternatively (to run experiments with separate participants): ####

With some difficulty. Out of the box you will need two windows machines, and equipment to establish a LAN connection (i.e. a router and some ethernet cables).

After connecting the two machines to the local network, you will need to start the game on both machines, and set one as the host (player 1) and the other as client (player 2).

This is accomplished using an input box that will appear in the upper left corner on startup. Host should click host, client should enter the host's IP address and click client.

If you mess up this process, you can press F5 (I know, I know... it is a bit messy), which will reset any stored configuration settings. 

Once you have successfully connected the machines, the game should save the config settings, so you won't have to repeat the process every time. 


### Who do I talk to? ###

Feel free to get a hold of me via cwjenk (A T) gmail if you have any questions - especially with any questions about my process or thoughts on the overall design of the game. 

I will not make any of the recordings / personal data from this project available publicly. Any access to these should be accomplished via official channels with the University of Stuttgart. 